package localhost.iillyyaa2033.maps.logic.storages;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import localhost.iillyyaa2033.maps.logic.items.LayerMeta;
import localhost.iillyyaa2033.maps.logic.items.Note;
import localhost.iillyyaa2033.maps.logic.items.Track;
import org.mapsforge.core.model.LatLong;

public class DatabaseStorage {
	
	final static String TABLE_NOTES = "notes";
	final static String COLUMN_NOTES_TAG = "tag";
	final static String COLUMN_NOTES_LAT = "lat";	// latitude
	final static String COLUMN_NOTES_LON = "lon";	// longitude
	final static String COLUMN_NOTES_COL = "col";	// color

	static String CREATE_TABLE_NOTES = "create table " + TABLE_NOTES + "(" +
	COLUMN_NOTES_TAG + " text not null, " +
	COLUMN_NOTES_LAT + " text not null, " +
	COLUMN_NOTES_LON + " text not null, " +
	COLUMN_NOTES_COL + " text not null" +
	");";

	final static String TABLE_LAYERS = "layers";
	final static String COLUMN_LAYERS_NAME = "name";
	final static String COLUMN_LAYERS_FILE = "file";

	static String CREATE_TABLE_LAYERS = "create table " + TABLE_LAYERS + "(" +
	COLUMN_LAYERS_NAME + " text not null, " +
	COLUMN_LAYERS_FILE + " text not null" +
	");";

	final static String TABLE_TRACKSMETA = "tracksmeta";
	final static String COLUMN_TRACKSMETA_ID = "id";
	final static String COLUMN_TRACKSMETA_NAME = "name";
	final static String COLUMN_TRACKSMETA_COLOR = "color";
	final static String COLUMN_TRACKSMETA_LENGHT = "lenght";
	final static String COLUMN_TRACKSMETA_TIME = "time";
	final static String COLUMN_TRACKSMETA_DISPLAY = "display";

	static String CREATE_TABLE_TRACKSMETA = "create table " + TABLE_TRACKSMETA + "(" +
	COLUMN_TRACKSMETA_ID + " text not null, " +
	COLUMN_TRACKSMETA_NAME + " text not null, " +
	COLUMN_TRACKSMETA_COLOR + " text not null, " +
	COLUMN_TRACKSMETA_LENGHT + " text not null, " +
	COLUMN_TRACKSMETA_TIME + " text not null, " +
	COLUMN_TRACKSMETA_DISPLAY + " text not null" +
	");";

	final static String TABLE_TRACKS = "tracks";
	final static String COLUMN_TRACKS_ID = "trackid";
	final static String COLUMN_TRACKS_TIME = "time";
	final static String COLUMN_TRACKS_LAT = "lat";
	final static String COLUMN_TRACKS_LONG = "long";

	static String CREATE_TABLE_TRACKS = "create table " + TABLE_TRACKS + "(" +
	COLUMN_TRACKS_ID + " text not null, " +
	COLUMN_TRACKS_TIME + " text not null, " +
	COLUMN_TRACKS_LAT + " text not null, " +
	COLUMN_TRACKS_LONG + " text not null" +
	");";

	public static ArrayList<Note> notes = new ArrayList<Note>();
	public static ArrayList<LayerMeta> layers = new ArrayList<LayerMeta>();
	public static ArrayList<Track> tracks = new ArrayList<Track>();
//	public static ArrayList<SearchItem> addrs = new ArrayList<SearchItem>();

	static SQLiteDatabase db;
	public static void openDB(Context c){
		MyDBOpenHelper helper = new MyDBOpenHelper(c, null);
		db = helper.getWritableDatabase();
	}
	
	public static void closeDB(){
		db.close();
	}
	
	public static synchronized void notes_add(Note n){
		ContentValues values = new ContentValues();
		values.put(COLUMN_NOTES_TAG, n.tag);
		values.put(COLUMN_NOTES_LAT, n.position.latitude);
		values.put(COLUMN_NOTES_LON, n.position.longitude);
		values.put(COLUMN_NOTES_COL, n.color);
		db.insert(TABLE_NOTES, null, values);
		
		notes.add(n);
	}
	public static synchronized void notes_remove(int position){
		Note n = notes.get(position);
		db.delete(TABLE_NOTES,COLUMN_NOTES_TAG+"='"+n.tag+"'"
				  +" and "+COLUMN_NOTES_COL+"='"+n.color+"'",
				  null);
		notes.remove(position);
	}
	
	public static synchronized void layers_add(LayerMeta m){
		if (db.isOpen() && m.isDeleteable) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_LAYERS_NAME, m.name);
			values.put(COLUMN_LAYERS_FILE, m.argument);
			db.insert(TABLE_LAYERS, null, values);
		}
		layers.add(m);
	}
	public static synchronized void layers_remove(int position){
		LayerMeta m = layers.get(position);
		db.delete(TABLE_LAYERS,COLUMN_LAYERS_NAME+"='"+m.name+"'",
			//+" and "+COLUMN_LAYERS_FILE+"='"+m.argument+"'",
			null);
		layers.remove(position);
	}
	
	public static synchronized void tracks_add(Track track){
		ContentValues values = new ContentValues();
		values.put(COLUMN_TRACKSMETA_ID, tracks.size());
		values.put(COLUMN_TRACKSMETA_NAME, track.name);
		values.put(COLUMN_TRACKSMETA_COLOR, track.color);
		values.put(COLUMN_TRACKSMETA_LENGHT, track.lenght);
		values.put(COLUMN_TRACKSMETA_TIME, track.time);
		values.put(COLUMN_TRACKSMETA_DISPLAY, track.mustBeDisplayed ? 1 : 0);
		db.insert(TABLE_TRACKSMETA, null, values);

		for (Track.TrackItem item : track.items) {
			ContentValues v2 = new ContentValues();
			v2.put(COLUMN_TRACKS_ID, tracks.size());
			v2.put(COLUMN_TRACKS_TIME, item.time);
			v2.put(COLUMN_TRACKS_LAT, item.position.latitude);
			v2.put(COLUMN_TRACKS_LONG, item.position.longitude);
			db.insert(TABLE_TRACKS, null, v2);
		}

		tracks.add(track);
	}
//	public static synchronized void tracks_remove(int position){}
	
	public static synchronized void update(Context cnt) {

		notes = new ArrayList<Note>();
		Cursor c;
		c = db.query(TABLE_NOTES, new String[]{COLUMN_NOTES_TAG,COLUMN_NOTES_LAT,COLUMN_NOTES_LON,COLUMN_NOTES_COL}, null, null, null, null, COLUMN_NOTES_TAG);
		if (c.moveToFirst()) {
			do {
				notes.add(new Note(c.getString(0), new LatLong(c.getDouble(1), c.getDouble(2)), c.getInt(3)));
			} while (c.moveToNext());
		}


		layers = new ArrayList<LayerMeta>();
		c = db.query(TABLE_LAYERS, new String[]{COLUMN_LAYERS_NAME,COLUMN_LAYERS_FILE}, null, null, null, null, null);
		if (c.moveToFirst()) {
			do {
				layers.add(new LayerMeta(c.getString(0), c.getString(1)));
			} while (c.moveToNext());
		}

		tracks = new ArrayList<Track>();
		c = db.query(TABLE_TRACKSMETA, new String[]{COLUMN_TRACKSMETA_ID,COLUMN_TRACKSMETA_NAME,COLUMN_TRACKSMETA_COLOR,COLUMN_TRACKSMETA_LENGHT,COLUMN_TRACKSMETA_TIME,COLUMN_TRACKSMETA_DISPLAY}, null, null, null, null, COLUMN_TRACKSMETA_ID);
		if (c.moveToFirst()) {
			do {
				tracks.add(new Track(c.getInt(0), c.getString(1), c.getInt(2), c.getInt(3), c.getLong(4), c.getInt(5)));
			} while (c.moveToNext());
		}

		c = db.query(TABLE_TRACKS, new String[]{COLUMN_TRACKS_ID,COLUMN_TRACKS_TIME,COLUMN_TRACKS_LAT,COLUMN_TRACKS_LONG}, null, null, null, null, COLUMN_TRACKS_ID);

		Track tr;
		if (tracks.size() > 0) tr = tracks.get(0);
		else tr = new Track();

		if (c.moveToFirst()) {
			do {
				int currId = c.getInt(0);
				if (currId != tr.id) {
					for (Track ntr : tracks) {
						if (ntr.id == currId) {
							tr = ntr;
							break;
						}
					}
				}
				tr.items.add(new Track.TrackItem(c.getLong(1), new LatLong(c.getDouble(2), c.getDouble(3))));
			} while (c.moveToNext());
		}

		for (Track track : tracks) {
			Track.TrackItem[] items = new Track.TrackItem[track.items.size()];
			track.items.toArray(items);
			Arrays.sort(items, new Comparator<Track.TrackItem>(){

					@Override
					public int compare(Track.TrackItem p1, Track.TrackItem p2) {
						if (p1.time == p2.time) return 0;
						if (p1.time < p2.time) return -1;
						if (p1.time > p2.time) return 1;
						return 0;
					}
				});
		}
	}

	static class MyDBOpenHelper extends SQLiteOpenHelper {

		static final String name = "awesomedb";
		static final int version = 1;


		public MyDBOpenHelper(Context context, SQLiteDatabase.CursorFactory factory) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_TABLE_NOTES);
			db.execSQL(CREATE_TABLE_LAYERS);
			db.execSQL(CREATE_TABLE_TRACKSMETA);
			db.execSQL(CREATE_TABLE_TRACKS);
		}

		@Override
		public void onUpgrade(SQLiteDatabase p1, int from, int to) {
			switch (from) {
				case 1:
					// DO UPGRADE FROM 1 TO 2
				case 2:
					// DO UPGRADE FROM 2 TO 3
					// ET CETERA
			}
		}
	}
}
