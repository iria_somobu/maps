package localhost.iillyyaa2033.maps.logic.items;

public class LayerMeta{
	
	public int id; // For database
	public String name;
	public boolean isDeleteable;	// means that layer stored in db
	public String argument;
	
	public LayerMeta(String name, boolean isDeleteable, String argument){	// for fragment
		this.name = name;
		this.isDeleteable = isDeleteable;
		this.argument = argument;
	}
	
	public LayerMeta(String name, String argument){	// for db
		this.name = name;
		isDeleteable = true;
		this.argument = argument;
	}

	@Override
	public boolean equals(LayerMeta o) {
		return this.name.equals(o.name)
			&& this.isDeleteable == o.isDeleteable
			&& this.argument.equals(o.argument);
	}
}
