package localhost.iillyyaa2033.maps.logic.maplayers;

import org.mapsforge.core.graphics.*;
import org.mapsforge.core.model.*;

import android.graphics.Color;
import localhost.iillyyaa2033.maps.logic.items.Note;
import localhost.iillyyaa2033.maps.logic.items.Track;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import org.mapsforge.core.util.MercatorProjection;
import org.mapsforge.map.layer.Layer;


public class PointsRendererLayer extends Layer {

	GraphicFactory graphicFactory;

    public PointsRendererLayer(GraphicFactory graphicFactory) {
		this.graphicFactory = graphicFactory;
	}

	@Override
	public void draw(BoundingBox boundingBox, byte zoomLevel, Canvas canvas, Point topLeftPoint) {

		for (Track t : DatabaseStorage.tracks) {
			if (t.mustBeDisplayed) drawTrack(canvas, t, zoomLevel, topLeftPoint);
		}

		for (Note n : DatabaseStorage.notes) {
			drawNote(canvas, n, zoomLevel, topLeftPoint);
		}

		if (SettingsStorage.ANDROID_MAP_LOCATION) drawLocationPoint(canvas, SettingsStorage.savedLocationLatLong, zoomLevel, topLeftPoint);
	}

	protected int getRadiusInPixels(int radius, double latitude, byte zoomLevel) {
        return (int) MercatorProjection.metersToPixels(radius, latitude, MercatorProjection.getMapSize(zoomLevel, displayModel.getTileSize()));
    }

	void drawLocationPoint(Canvas canvas, LatLong point, byte zoomLevel, Point topLeftPoint) {
		double latitude = point.latitude;
		double longitude = point.longitude;
		long mapSize = MercatorProjection.getMapSize(zoomLevel, displayModel.getTileSize());
		int pixelX = (int) (MercatorProjection.longitudeToPixelX(longitude, mapSize) - topLeftPoint.x);
		int pixelY = (int) (MercatorProjection.latitudeToPixelY(latitude, mapSize) - topLeftPoint.y);
		int radiusInPixel = getRadiusInPixels(100, latitude, zoomLevel);

		Rectangle canvasRectangle = new Rectangle(0, 0, canvas.getWidth(), canvas.getHeight());
		if (!canvasRectangle.intersectsCircle(pixelX, pixelY, radiusInPixel)) {
			return;
		}

		Paint paintFill = graphicFactory.createPaint();
		paintFill.setColor(Color.parseColor("#25B60A00"));
		canvas.drawCircle(pixelX, pixelY, radiusInPixel, paintFill);

		Paint paintStroke = graphicFactory.createPaint();
		paintStroke.setStyle(Style.STROKE);
		paintStroke.setStrokeWidth(3f);
		paintStroke.setColor(Color.parseColor("#BBB60A00"));
		canvas.drawCircle(pixelX, pixelY, radiusInPixel, paintStroke);
	}

	void drawNote(Canvas canvas, Note note, byte zoomLevel, Point topLeftPoint) {
		double latitude = note.position.latitude;
		double longitude = note.position.longitude;
		long mapSize = MercatorProjection.getMapSize(zoomLevel, displayModel.getTileSize());
		int pixelX = (int) (MercatorProjection.longitudeToPixelX(longitude, mapSize) - topLeftPoint.x);
		int pixelY = (int) (MercatorProjection.latitudeToPixelY(latitude, mapSize) - topLeftPoint.y);
		int radiusInPixel = getRadiusInPixels(20, latitude, zoomLevel);

		Rectangle canvasRectangle = new Rectangle(0, 0, canvas.getWidth(), canvas.getHeight());
		if (!canvasRectangle.intersectsCircle(pixelX, pixelY, radiusInPixel)) {
			return;
		}

		Paint paint = graphicFactory.createPaint();
		paint.setColor(note.color);
		paint.setBitmapShaderShift(topLeftPoint);
		canvas.drawCircle(pixelX, pixelY, radiusInPixel, paint);
	}

	void drawTrack(Canvas canvas, Track track, byte zoomLevel, Point topLeftPoint) {
		Rectangle canvasRectangle = new Rectangle(0, 0, canvas.getWidth(), canvas.getHeight());

		int radiusInPixel = getRadiusInPixels(20, 0f, zoomLevel);

		Paint paint = graphicFactory.createPaint();
		paint.setColor(track.color);
		paint.setBitmapShaderShift(topLeftPoint);
		paint.setStrokeWidth(radiusInPixel);
		
		boolean isFirst = true;
		int prevPixelX = -1;
		int prevPixelY = -1;
		for (Track.TrackItem t : track.items) {
			double latitude = t.position.latitude;
			double longitude = t.position.longitude;
			long mapSize = MercatorProjection.getMapSize(zoomLevel, displayModel.getTileSize());
			int pixelX = (int) (MercatorProjection.longitudeToPixelX(longitude, mapSize) - topLeftPoint.x);
			int pixelY = (int) (MercatorProjection.latitudeToPixelY(latitude, mapSize) - topLeftPoint.y);

			// TODO: optimisation
		/*	if (!canvasRectangle.intersectsCircle(pixelX, pixelY, radiusInPixel)) {
				if(prevPixelX != -1) 
				continue;
			}	*/

			if(isFirst) isFirst = false;
			else canvas.drawLine(prevPixelX,prevPixelY,pixelX,pixelY,paint);
			
			canvas.drawCircle(pixelX, pixelY, radiusInPixel, paint);
			
			prevPixelX = pixelX;
			prevPixelY = pixelY;
		}
	}
}
