package localhost.iillyyaa2033.maps.fragment;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import java.util.ArrayList;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.logic.items.Track;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;
import org.mapsforge.core.model.LatLong;

public class TrackerFragment extends Fragment{
	
/*	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
            closer = (OnFragmentCloseListener) activity;
			layers = (LayerManagerInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement some interfaces");
        }
	} */
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.fragment_tracker, container, false);
		
		ArrayList<String> names = new ArrayList<String>();
		for(Track track: DatabaseStorage.tracks){
			names.add(track.name);
		}
		
		ListView list = (ListView) ll.findViewById(R.id.fragment_tracker_list);
		
		// TODO: custom adapter with track color, distance and time support
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,names);
		list.setAdapter(adapter);
		
		return ll;
	}
}
