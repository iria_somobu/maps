package localhost.iillyyaa2033.maps.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.activity.EditLayerActivity;
import localhost.iillyyaa2033.maps.adapter.LayersManagerAdapter;
import localhost.iillyyaa2033.maps.interfaces.MapActivityInterface;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;

public class LayersManagerFragment extends Fragment {

	LayersManagerAdapter adapter;
	MapActivityInterface activity;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
            this.activity = (MapActivityInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement some interfaces");
        }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.fragment_layersmanager, container, false);

		DragSortListView list = (DragSortListView) ll.findViewById(R.id.fragment_layersmanager_ListView1);
		DragSortController controller = new DragSortController(list);
		// note: sorting cause many bugs, so it was disabled; handle visiblity now == gone
		controller.setDragHandleId(R.id.drag_handle);
		controller.setRemoveEnabled(true);
		controller.setSortEnabled(!true);
		controller.setDragInitMode(DragSortController.ON_DOWN);
		controller.setRemoveMode(DragSortController.FLING_REMOVE);
		controller.setBackgroundColor(Color.argb(50, 0, 0, 0));
		list.setFloatViewManager(controller);
		list.setOnTouchListener(controller);
		list.setDragEnabled(true);
		list.setOnItemLongClickListener(new OnItemLongClickListener(){

				@Override
				public boolean onItemLongClick(AdapterView<?> p1, View p2, int p3, long p4) {
					if (DatabaseStorage.layers.get(p3).isDeleteable) {
						Intent i = new Intent(getActivity(), EditLayerActivity.class);
						i.putExtra("layerid", p3);
						startActivityForResult(i, 0);
						return true;
					} else {
						return false;
					}
				}
			});
		list.setDropListener(new DragSortListView.DropListener() {

				@Override
				public void drop(int from, int to) {
					if (from != to) {
						/*             LayerMeta item = adapter.getItem(from);
						 DatabaseStorage.layers.remove(item);
						 DatabaseStorage.layers.add(to,item);
						 adapter.notifyDataSetChanged();

						 layers.resyncLayers();
						 layers.redrawLayers();	*/
					}
				}
			});
		list.setRemoveListener(new DragSortListView.RemoveListener(){

				@Override
				public void remove(int which) {
					DatabaseStorage.layers_remove(which);
					adapter.notifyDataSetChanged();
					activity.resyncLayers();
				}
			});

		adapter = new LayersManagerAdapter(getActivity());
		list.setAdapter(adapter);

		Button addBtn = (Button) ll.findViewById(R.id.fragment_layersmanager_Button1);
		addBtn.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					startActivityForResult(new Intent(getActivity(), EditLayerActivity.class), 0);
				}
			});
		return ll;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			activity.resyncLayers();
			adapter.notifyDataSetChanged();
		}
	}
}
