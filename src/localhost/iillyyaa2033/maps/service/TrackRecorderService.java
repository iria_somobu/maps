package localhost.iillyyaa2033.maps.service;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.activity.StopTrackRecorderActivity;
import localhost.iillyyaa2033.maps.logic.items.Track;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import org.mapsforge.core.model.LatLong;

public class TrackRecorderService extends Service implements LocationListener {

	Track currentTrack;
	LocationManager man;
	
	@Override
	public void onCreate() {
		super.onCreate();
		toForeground();
		currentTrack = new Track();
		int i = android.R.drawable.ic_menu_help;
		man = (LocationManager) getSystemService(Activity.LOCATION_SERVICE);
		if(SettingsStorage.providerForTracker == null){
			stopSelf();
			return;
		}
		
	//	man.removeUpdates(this);
		man.requestLocationUpdates(SettingsStorage.providerForTracker, 2 * 60 * 1000, 0, this);
	}
	
	@Override
	public IBinder onBind(Intent p1) {
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Toast.makeText(this,"Here will be saving to db or so",Toast.LENGTH_SHORT).show();
		if(currentTrack.items.size()>0) DatabaseStorage.tracks_add(currentTrack);
		man.removeUpdates(this);
	}
	
	public void toForeground(){
		int NOTIFICATION_ID = 1; 
		Intent intent = new Intent(this, StopTrackRecorderActivity.class); 
		PendingIntent pi = PendingIntent.getActivity(this, 1, intent, 0);
		Notification notification = new Notification(R.drawable.ic_launcher, "Running in the Foreground", System.currentTimeMillis()); 
		notification.setLatestEventInfo(this, "TrackRecorder running", "Tap here to finish it", pi); 
		notification.flags = notification.flags | Notification.FLAG_ONGOING_EVENT; 
		startForeground(NOTIFICATION_ID, notification);
	}
	
	@Override
	public void onLocationChanged(Location p1) {
		Track.TrackItem item = new Track.TrackItem(p1.getTime(),new LatLong(p1.getLatitude(),p1.getLongitude()));
		currentTrack.items.add(item);
	}

	@Override
	public void onStatusChanged(String p1, int p2, Bundle p3) {
		// TODO: Implement this method
	}

	@Override
	public void onProviderEnabled(String p1) {
		// TODO: Implement this method
	}

	@Override
	public void onProviderDisabled(String p1) {
		// TODO: Implement this method
	}
}
