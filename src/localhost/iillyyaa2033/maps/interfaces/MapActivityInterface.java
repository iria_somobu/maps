package localhost.iillyyaa2033.maps.interfaces;

import java.util.ArrayList;
import localhost.iillyyaa2033.maps.logic.items.SearchItem;
import org.mapsforge.core.model.LatLong;

public interface MapActivityInterface{
	
	public void redrawLayers();
	public void resyncLayers();
	public void scroll(LatLong to);
	public void scroll(LatLong to, boolean immediately);
	
	public void toggleBars();
	
	public void closePanel();
	
	public void listenFrom(String provider);
	public void startRecordTrack(String provider);
	
	public void addNote(LatLong position);
	
	public void search(String request, boolean inDatabase);
	
	public void startLayerman();
}
