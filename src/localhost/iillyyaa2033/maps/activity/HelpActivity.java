package localhost.iillyyaa2033.maps.activity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class HelpActivity extends Activity {

	WebView view;
	String page;
	
	String[] titles4Panels = {
		"PANEL_LOCATION",
		"PANEL_NOTES",
		"PANEL_LAYERS_MANAGER",
		"PANEL_SEARCH",
		"PANEL_TRACKER"
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		view = new WebView(this);
		setContentView(view);
		
		page = getIntent().getStringExtra("page");
		view.loadData("requseted page: "+page,null,null);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		String data = "<html><body>";
		
		if(page.startsWith("panel_")){
	//		data += "<h3>"+titles4Panels[Integer.getInteger(page.substring(5))]+"</h3>";
		}
		
		data += "<i>Страница справки не найдена</i></body></html>";
		view.loadData(data,"text/html;charset=utf8",null);
	}
}
