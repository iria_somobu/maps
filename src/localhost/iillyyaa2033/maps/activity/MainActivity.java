package localhost.iillyyaa2033.maps.activity;

import android.app.*;
import android.location.*;
import android.view.*;
import android.widget.*;
import localhost.iillyyaa2033.maps.fragment.*;
import localhost.iillyyaa2033.maps.interfaces.*;
import localhost.iillyyaa2033.maps.logic.storages.*;

import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.logic.items.SearchItem;
import localhost.iillyyaa2033.maps.service.TrackRecorderService;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.Tag;
import org.mapsforge.map.datastore.Way;
import org.osmdroid.util.GeoPoint;
import localhost.iillyyaa2033.maps.service.MapOverlayService;

public class MainActivity extends Activity implements LocationListener, OnSearchListener, MapActivityInterface {

	FrameLayout primary;
	LinearLayout secondary;
	Fragment mapFragment;
	MapFragmentInterface mapInterface;
	int lastOpenedPanel;
	final int REQUEST_PANEL = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_loading);
		startService(new Intent(this,MapOverlayService.class));
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		SettingsStorage.updateAll(this);
		DatabaseStorage.openDB(this);
		DatabaseStorage.update(this);

		setContentView(R.layout.activity_main);
		primary = (FrameLayout) findViewById(R.id.activity_main_frame_primary);
		secondary = (LinearLayout) findViewById(R.id.activity_main_panel);

		if (secondary != null) {
			secondary.setVisibility(View.GONE);
			ImageButton btn = (ImageButton) findViewById(R.id.activity_main_panel_close);
			btn.setOnClickListener(new View.OnClickListener(){

					@Override
					public void onClick(View p1) {
						openPanel(PANEL_NOTHING);
					}
				});

			ImageButton help = (ImageButton) findViewById(R.id.activity_main_panel_help);
			help.setOnClickListener(new View.OnClickListener(){

					@Override
					public void onClick(View p1) {
						openHelpForPanel(lastOpenedPanel);
					}
				});
		} 


		OsmdroidMapFragment frag = new OsmdroidMapFragment();
		mapFragment = frag;
		mapInterface = frag;
		getFragmentManager()
			.beginTransaction()
			.add(R.id.activity_main_frame_primary, mapFragment)
			.commit();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (!barsShown) hideBars();
		mapInterface.setLocationEnabled(SettingsStorage.ANDROID_MAP_LOCATION);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mapInterface.onPause();
	}

	@Override
	protected void onStop() {
		SettingsStorage.saveAll(this);
//		DatabaseStorage.closeDB();
		super.onStop();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (data.getBooleanExtra("redrawLayers", false)) redrawLayers();
			if (data.getBooleanExtra("resyncLayers", false)) resyncLayers();
			if (data.getBooleanExtra("scroll", false)) {
				double lat = data.getDoubleExtra("scrollX", SettingsStorage.savedViewLatLong.latitude);
				double lon = data.getDoubleExtra("scrollY", SettingsStorage.savedViewLatLong.longitude);
				scroll(new LatLong(lat, lon));
			}
			if (data.getBooleanExtra("listenFrom", false)) {
				listenFrom(data.getStringExtra("locationProvider"));
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (SettingsStorage.ANDROID_BEHAVIOR_ZOOM_BY_VOLUME) {
			if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
				mapInterface.zoomIn();
				return true;
			} 
			if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
				mapInterface.zoomOut();
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, R.string.android_activity_main_ab_location).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		menu.add(0, 1, 9, R.string.android_activity_main_ab_preferences).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		menu.add(0, 2, 2, R.string.android_activity_main_ab_notes).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		menu.add(0, 3, 3, R.string.android_activity_main_ab_layers).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		menu.add(0, 4, 4, R.string.android_activity_main_ab_search).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		menu.add(0, 5, 5, R.string.android_activity_main_ab_tracker).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case 0:
				openPanel(PANEL_LOCATION);
				break;
			case 1:
				startActivity(new Intent(this, PreferenceAndroidActivity.class));
				break;
			case 2:
				openPanel(PANEL_NOTES);
				break;
			case 3:
				openPanel(PANEL_LAYERS_MANAGER);
				break;
			case 4:
				openPanel(PANEL_SEARCH);
				break;
			case 5:
				openPanel(PANEL_TRACKER);
				break;
		}
		return true;
	}

	public void openPanel(int which) {
		lastOpenedPanel = which;

		if (secondary == null) {
			if (which == 0) return;
			Intent i = new Intent(MainActivity.this, MainActivityPanel.class);
			i.putExtra("panelNum", which);
			if (which == PANEL_SEARCH) LocalSearchedBuffer.items = mapInterface.getVisibleWays();
			startActivityForResult(i, REQUEST_PANEL + which);
		} else {
			int replaceId = R.id.activity_main_frame_secondary;
			TextView title = (TextView) findViewById(R.id.activity_main_panel_title);
			FragmentTransaction transaction = getFragmentManager().beginTransaction();

			switch (which) {
				case PANEL_NOTHING:
					secondary.setVisibility(View.GONE);
					transaction.replace(replaceId, new Fragment());
					title.setText("");
					break;
				case PANEL_LOCATION:
					title.setText(R.string.android_fragment_location_provider_title);
					secondary.setVisibility(View.VISIBLE);
					transaction.replace(replaceId, new LocationFragment());
					break;
				case PANEL_NOTES:
					title.setText(R.string.android_dialog_notes_list_title);
					secondary.setVisibility(View.VISIBLE);
					transaction.replace(replaceId, new NotesListFragment());
					break;
				case PANEL_LAYERS_MANAGER:
					title.setText(R.string.android_fragment_layersman_title);
					secondary.setVisibility(View.VISIBLE);
					transaction.replace(replaceId, new MapsManagerFragment());
					break;
				case PANEL_SEARCH:
					title.setText(R.string.android_fragment_search_title);
					secondary.setVisibility(View.VISIBLE);
					transaction.replace(replaceId, new SearchFragment());
					break;
				case PANEL_TRACKER:
					title.setText(R.string.android_fragment_tracker_title);
					secondary.setVisibility(View.VISIBLE);
					transaction.replace(replaceId, new TrackerFragment());
					break;
			}

			transaction.commit();
		}
	}

	public void openHelpForPanel(int which) {
		Intent i = new Intent(this, HelpActivity.class);
		i.putExtra("page", "panel_" + which);
		startActivity(i);
	}

	public static final int PANEL_NOTHING = 0;
	public static final int PANEL_LOCATION = 1;
	public static final int PANEL_NOTES = 2;
	public static final int PANEL_LAYERS_MANAGER = 3;
	public static final int PANEL_SEARCH = 4;
	public static final int PANEL_TRACKER = 5;


	//
	// LOCATION LISTENER
	//

	@Override
	public void onLocationChanged(Location p1) {
		SettingsStorage.savedLocationLatLong = new LatLong(p1.getLatitude(), p1.getLongitude());
		mapInterface.setNewLocation(new GeoPoint(p1.getLatitude(),p1.getLongitude()),true);
		if (SettingsStorage.ANDROID_BEHAVIOR_SCROLL_TO_LOCATION) mapInterface.scrollTo(SettingsStorage.savedLocationLatLong);
	}

	@Override
	public void onStatusChanged(String p1, int p2, Bundle p3) {

	}

	@Override
	public void onProviderEnabled(String p1) {

	}

	@Override
	public void onProviderDisabled(String p1) {
		startActivity(new Intent(this, StopTrackRecorderActivity.class));
		mapInterface.setNewLocation(new GeoPoint(SettingsStorage.savedLocationLatLong.getLatitude(),SettingsStorage.savedLocationLatLong.getLongitude()),false);
	}

	//
	// LAYER MANAGER INTERFACE
	//

	@Override
	public void redrawLayers() {
	//	mapInterface.redrawLayers();
	}

	@Override
	public void resyncLayers() {
		mapInterface.resyncLayers();
	}

	//
	// ON FRAGMENT CLOSE LISTENER
	//

	@Override
	public void closePanel() {
		openPanel(PANEL_NOTHING);
	}

	//
	// ON MAP SCROLL REQUESTED LISTENER
	//

	@Override
	public void scroll(LatLong to) {
		mapInterface.scrollTo(to);
	}

	@Override
	public void scroll(LatLong to, boolean immediately) {
		scroll(to);
	}

	@Override
	public void listenFrom(String provider) {
		LocationManager man = (LocationManager) getSystemService(Activity.LOCATION_SERVICE);
		man.removeUpdates(this);
		man.requestLocationUpdates(provider, 0, 0, this);
	}

	@Override
	public void startRecordTrack(String provider) {
		if (!SettingsStorage.isTrackRecorderStarted) {
			SettingsStorage.providerForTracker = provider;
			startService(new Intent(this, TrackRecorderService.class));
			SettingsStorage.isTrackRecorderStarted = true;
		}
	}

	@Override
	public void addNote(LatLong position) {
		Intent i = new Intent(MainActivity.this, EditNoteActivity.class);
		i.putExtra("positionX", position.latitude);
		i.putExtra("positionY", position.longitude);
		// TODO: startActivityForResult
		startActivity(i);
	}

	@Override
	public void startLayerman() {
		startActivityForResult(new Intent(this, MainActivityDialog.class), 0);
	}

	//
	// ON SEARCH LISTENER
	//
	OnSearchListener.OnSearchResult searchListener;

	@Override
	public void search(String request, boolean inDatabase) {

		request = request.toLowerCase();

		ArrayList<SearchItem> list = new ArrayList<SearchItem>();

		List<Way> allNotes = mapInterface.getVisibleWays();
		String name = null;
		for (Way w : allNotes) {
			if ((name = haveTag(w, "name")) != null) {
				if (name.toLowerCase().contains(request)) {
					SearchItem itm = new SearchItem(name, w.latLongs);
					list.add(itm);
				}
			}
		}
		searchListener.onResult(list);
	}

	String haveTag(Way w, String tag) {
		for (Tag t : w.tags) {
			if (t.key.equals(tag)) return t.value;
		}
		return null;
	}

	@Override
	public void setOnSearchListener(OnSearchListener.OnSearchResult listener) {
		searchListener = listener;
	}

	boolean barsShown = true;
	@Override
	public void toggleBars() {
		if (barsShown) hideBars();
		else showBars();
		barsShown = !barsShown;
	}

	void hideBars() {

		int version = android.os.Build.VERSION.SDK_INT;
		if (version < 19) {
			getActionBar().hide();
			return;
		}

		if (version < 99) {
			try {
				getWindow().getDecorView().setSystemUiVisibility( 
					View.SYSTEM_UI_FLAG_LAYOUT_STABLE | 
					View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | 
					View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 
					View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | 
					View.SYSTEM_UI_FLAG_FULLSCREEN | 
					View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
			} catch (Exception e) {
				// TODO: report about exception
			}
		}
	}

	void showBars() {
		int version = android.os.Build.VERSION.SDK_INT;
		if (version < 19) {
			getActionBar().show();
			return;
		}

		if (version < 99) {
			try {
				getWindow().getDecorView().setSystemUiVisibility(0);
				//		View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
				//		View.SYSTEM_UI_FLAG_VISIBLE);
			} catch (Exception e) {
				// TODO: report about exception
			}
		}
	}
}
