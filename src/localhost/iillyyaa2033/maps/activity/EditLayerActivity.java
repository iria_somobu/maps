package localhost.iillyyaa2033.maps.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import java.io.File;
import localhost.iillyyaa2033.maps.R;
import localhost.iillyyaa2033.maps.logic.items.LayerMeta;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;
import android.widget.EditText;
import android.content.Intent;
import android.net.Uri;
import android.content.ActivityNotFoundException;

public class EditLayerActivity extends Activity {

	EditText path, name;
	int layerid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_editlayer);

		layerid = getIntent().getIntExtra("layerid", -1);

		path = (EditText) findViewById(R.id.activity_editlayer_path);
		name = (EditText) findViewById(R.id.activity_editlayer_name);
		if (layerid >= 0 && layerid < DatabaseStorage.layers.size()) {
			LayerMeta meta = DatabaseStorage.layers.get(layerid);
			path.setText(meta.argument);
			name.setText(meta.name);
		}

		((Button) findViewById(R.id.activity_editlayer_pick)).setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					try {
						Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
						intent.setType("file/*");
						startActivityForResult(intent, 0);
					} catch (ActivityNotFoundException e) {
						Toast.makeText(EditLayerActivity.this,e.toString(),Toast.LENGTH_LONG).show();
					}
				}
			});
		((Button) findViewById(R.id.activity_editlayer_accept)).setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					if (new File(path.getText().toString()).exists()) {
						if(layerid >= 0 && layerid < DatabaseStorage.layers.size()) 
							DatabaseStorage.layers_remove(layerid);
						
						LayerMeta meta = new LayerMeta(name.getText().toString(), true, path.getText().toString());
						DatabaseStorage.layers_add(meta);
						setResult(Activity.RESULT_OK);
						finish();
					} else {
						Toast.makeText(EditLayerActivity.this, "File doesn't exist", Toast.LENGTH_SHORT).show();
					}
				}
			});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			path.setText(data.getData().getPath());
		}
	}
}
