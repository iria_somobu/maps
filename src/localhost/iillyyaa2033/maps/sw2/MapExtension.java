package localhost.iillyyaa2033.maps.sw2;

import android.graphics.*;

import android.app.Service;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import com.sonyericsson.extras.liveware.aef.control.Control;
import com.sonyericsson.extras.liveware.extension.util.control.ControlExtension;
import com.sonyericsson.extras.liveware.extension.util.control.ControlTouchEvent;
import localhost.iillyyaa2033.maps.logic.storages.DatabaseStorage;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import localhost.iillyyaa2033.maps.service.MapOverlayService;


public class MapExtension extends ControlExtension {

	public static final int width = 220;
	public static final int height = 176;

	MapView mapView;
	Service cnt;
	Updater u;

	Bundle[] menuItems = new Bundle[3];
	private static final int MENU_ITEM_0 = 0;
	private static final int MENU_ITEM_1 = 1;
	private static final int MENU_ITEM_2 = 2;
	boolean menuNeeded = false;
	boolean activeLowPower = false;

	public Handler handler = new Handler();
	public long lastUpdated = 0;

	public MapExtension(Service cnt, String hostAppName) {
		super(cnt, hostAppName);
		this.cnt = cnt;

		SettingsStorage.updateAll(cnt);
		DatabaseStorage.openDB(cnt);
		DatabaseStorage.update(cnt);

		menuItems[0] = new Bundle();
        menuItems[0].putInt(Control.Intents.EXTRA_MENU_ITEM_ID, MENU_ITEM_0);
        menuItems[0].putString(Control.Intents.EXTRA_MENU_ITEM_TEXT, "Toggle low power");

        menuItems[1] = new Bundle();
        menuItems[1].putInt(Control.Intents.EXTRA_MENU_ITEM_ID, MENU_ITEM_1);
        menuItems[1].putString(Control.Intents.EXTRA_MENU_ITEM_TEXT, "Item 2");

        menuItems[2] = new Bundle();
        menuItems[2].putInt(Control.Intents.EXTRA_MENU_ITEM_ID, MENU_ITEM_2);
        menuItems[2].putString(Control.Intents.EXTRA_MENU_ITEM_TEXT, "Item 3");
	}

	@Override
	public void onStart() {
		mapView = new MapView(cnt);
		int widthSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
		int heightSpec = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);
		mapView.measure(widthSpec, heightSpec);
		mapView.layout(0, 0, width, height);

		mapView.getController().setCenter(new GeoPoint(SettingsStorage.savedViewLatLong.latitude, SettingsStorage.savedViewLatLong.longitude));
		mapView.getController().setZoom(SettingsStorage.savedZoomLevel);
		mapView.getController().zoomTo(SettingsStorage.savedZoomLevel);
		mapView.setBuiltInZoomControls(SettingsStorage.ANDROID_MAP_ZOOMCONTROLS);

		resyncLayers();
		
		u = new Updater();
		u.setDaemon(true);
		u.start();
	}

	@Override
	public void onResume() {
		setScreenState(Control.Intents.SCREEN_STATE_AUTO);
		draw();
	}

	@Override
	public void onActiveLowPowerModeChange(boolean lowPowerMode) {
		activeLowPower = lowPowerMode;
	}

	@Override
	public void onSwipe(int direction) {
		super.onSwipe(direction);

		switch (direction) {
			case Control.Intents.SWIPE_DIRECTION_UP:
				scrollBy(0, -width / 3);
				break;
			case Control.Intents.SWIPE_DIRECTION_DOWN:
				scrollBy(0, width / 3);
				break;
			case Control.Intents.SWIPE_DIRECTION_LEFT:
				scrollBy(-height / 3, 0);
				break;
			case Control.Intents.SWIPE_DIRECTION_RIGHT:
				scrollBy(height / 3, 0);
				break;
		}
	}

	@Override
	public void onTouch(ControlTouchEvent event) {

		switch (event.getAction()) {
			case Control.Intents.TOUCH_ACTION_RELEASE:
				zoomIn();
				break;
			case Control.Intents.TOUCH_ACTION_LONGPRESS:
				zoomOut();
				break;
		}
	}

	@Override
	public void onKey(int action, int keyCode, long timeStamp) {
		if (action == Control.Intents.KEY_ACTION_RELEASE) {
			if (keyCode == Control.KeyCodes.KEYCODE_OPTIONS) {
				menuNeeded = ! menuNeeded;
				draw();

				if (activeLowPower) setScreenState(Control.Intents.SCREEN_STATE_AUTO);
				else setScreenState(Control.Intents.SCREEN_STATE_OFF);
			} else if (keyCode == Control.KeyCodes.KEYCODE_BACK) {
				stopRequest();
			}
		}
	}

	@Override
	public void onMenuItemSelected(int menuItem) {
		switch (menuItem) {
			case MENU_ITEM_0:
				if (activeLowPower) setScreenState(Control.Intents.SCREEN_STATE_AUTO);
				else setScreenState(Control.Intents.SCREEN_STATE_OFF);
				break;
			case MENU_ITEM_1:
			case MENU_ITEM_2:
				stopRequest();
		}
	}

	public void resyncLayers() {

		mapView.setUseDataConnection(true);
		final ITileSource source = TileSourceFactory.MAPNIK;
		mapView.setTileSource(source);
	}

	void draw() {
		if (menuNeeded) {
			showMenu(menuItems);
			menuNeeded = !menuNeeded;
		} else {
			

			if (MapOverlayService.bm == null) {
				Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
				//	bmp.compress(Bitmap.CompressFormat.PNG, 100, new ByteArrayOutputStream(256));
				bmp.setDensity(DisplayMetrics.DENSITY_MEDIUM);

				Canvas cnv = new Canvas(bmp);
				Paint p = new Paint();
				p.setColor(Color.WHITE);
				
				mapView.invalidate();
				mapView.draw(cnv);
				
				cnv.drawText((MapOverlayService.map==null?"internal":"external") +" at " + lastUpdated, 10, 10, p);

				showBitmap(bmp);
			}else{
				MapOverlayService.draw();
				showBitmap(MapOverlayService.bm);
			}
			
			lastUpdated = System.currentTimeMillis();
		}
	}

	public void zoomIn() {
		mapView.getController().zoomIn();
	}

	@Override
	public void zoomOut() {
		mapView.getController().zoomOut();
	}

	@Override
	public void scrollTo(double latitude, double longitude) {
		mapView.getController().setCenter(new GeoPoint(latitude, longitude));
	}

	public void scrollBy(int horizontal, int vertical) {
		mapView.getController().scrollBy(horizontal, vertical);
		draw();
	}

	class Updater extends Thread {

		@Override
		public void run() {
			while (!isInterrupted()) {
				try {
					sleep(1 * 1000);
				} catch (InterruptedException e) {

				}

				long now = System.currentTimeMillis();
				if ((now - lastUpdated) > 1 * 1000) {
					handler.post(new Runnable(){

							@Override
							public void run() {
								draw();
							}
						});
				}
			}
		}
	}
}
