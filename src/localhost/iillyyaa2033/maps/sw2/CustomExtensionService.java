package localhost.iillyyaa2033.maps.sw2;

import com.sonyericsson.extras.liveware.extension.util.ExtensionService;
import com.sonyericsson.extras.liveware.extension.util.control.ControlExtension;
import com.sonyericsson.extras.liveware.extension.util.registration.DeviceInfoHelper;
import com.sonyericsson.extras.liveware.extension.util.registration.RegistrationInformation;
import localhost.iillyyaa2033.maps.logic.storages.SettingsStorage;

public class CustomExtensionService extends ExtensionService {

	@Override
	protected RegistrationInformation getRegistrationInformation() {
		return new CustomRegistrationInformation(this);
	}

	@Override
	protected boolean keepRunningWhenConnected() {
		return false;
	}

	@Override
	public ControlExtension createControlExtension(String hostAppPackageName) {
		
        boolean isSw2Supported = DeviceInfoHelper.isSmartWatch2ApiAndScreenDetected(this, hostAppPackageName);
        if (isSw2Supported) {
            return new MapExtension(this,hostAppPackageName);
        } else {
            throw new IllegalArgumentException("No control for: " + hostAppPackageName);
        }
	}
}
